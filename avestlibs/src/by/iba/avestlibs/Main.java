package by.iba.avestlibs;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import by.avest.crypto.pkcs11.provider.AvestProvider;
import by.avest.crypto.pkcs11.provider.ProviderFactory;
import by.avest.crypto.pkcs.pkcs7.PKCS7;
import by.avest.crypto.pkcs.pkcs7.SignerInfo;
import by.avest.vdk.FileUtilities;

public class Main {

	final static private String[] sign = { "decl.p7s", "insp.p7s", "serv.p7s" };

	public static void main(String[] args) throws IOException,
			NoSuchAlgorithmException, SignatureException {
		AvestProvider prov = ProviderFactory.addAvTokenProvider();
		// ISignedDocument doc = SignedDocumentFactory.getSignedDocument
		for (String s : sign) {
			byte[] signed = FileUtilities.loadFromFile(new File(s));
			PKCS7 pkcs7 = new PKCS7(signed);
			int i = 0;
			System.out.println("Checking signature in " + s);
			for (SignerInfo si : pkcs7.getSignerInfos()) {
				System.out.println("Checking signature#" + i++);
				// verify each signature
				boolean isValid = pkcs7.verify(si, null) != null;
				if (isValid) {
					System.out.println("Signature is valid");
				} else {
					System.out.println("Signature is NOT valid");
				}

				// verify the signer certificate
				// however date could be obtained from other sources like TSA
				// (trusted), OCSP (trusted), SigningTimeAttribute from PKCS7
				// message (untrusted)
			}
		}
		prov.close();

	}

}
